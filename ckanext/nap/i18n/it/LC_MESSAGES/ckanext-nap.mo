��    =                    �     �     �     �                 	        $     -     2     9     ?     E     M     \     a     f     k     p     �  
   �     �  	   �     �     �     �     �     �     �  	   �  	   �     �     �       	   	                    6     ;  	   @     J     P     ^     f     r     y     �     �     �  	   �     �     �     �     �                     *     0  �  9  6   �        
             &     .  
   7     B     O     X     a  
   i     t     �     �     �     �     �     �     �  
   	     	     1	     L	     g	     n	  
   �	  1   �	     �	  	   �	     �	     �	     �	     �	     �	     
     
     -
     K
  	   `
  	   j
     t
     y
     �
     �
     �
     �
     �
     �
     �
     �
       	             *     /     8     T     r     w   AGRI ANNUAL ANNUAL_2 ANNUAL_3 Apply BIENNIAL BIMONTHLY BIWEEKLY CONT Cancel Clear DAILY DAILY_2 Dataset extent ECON EDUC ENER ENVI End of temporal extent Field Information Field Name Filter by location Frequency GOVE HEAL INTR IRREG JUST MONTHLY MONTHLY_2 MONTHLY_3 NEVER OTHER Order QUARTERLY REGI SOCI Start of temporal extent TECH TRAN TRIENNIAL Theme Type (Format) UNKNOWN UPDATE_CONT WEEKLY WEEKLY_2 WEEKLY_3 category fiscal_code frequency main_website_url operatein_territory_names operatein_territory_type organization_type rights_holder temporal_end temporal_start theme vat_code Project-Id-Version: ckanext-mit 0.0.1
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-12-01 16:42+0000
PO-Revision-Date: 2021-12-01 16:48+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: it
Language-Team: it <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.7.0
 Agricoltura, pesca, silvicoltura e prodotti alimentari Annuale Semestrale Tre volte all'anno Applica Biennale Binestrale Quindicinale Continuo Cancella Pulisci Quotidiano Due volte al giorno Estensione geografica Economia e finanze Istruzione, cultura e sport Energia Ambiente Estensione temporale (fine) Informazioni sui campi Nome campo Filtra per posizione Frequenza di aggiornamento Governo e settore pubblico Salute Tematiche internazionali Irregolare Giustizia, sistema giuridico e sicurezza pubblica Mensile Bimensile Tre volte al mese Mai Altro Ordine Trimestrale Regioni e città Popolazione e società Estensione temporale (inizio) Scienza e tecnologia Trasporti Triennale Tema Formato Sconosciuto In continuo aggiornamento Settimanale Bisettimanale Tre volte a settimanaa Tema Codice fiscale Frequenza di aggiornamento Sito web Territori Tipo territorio Tipo Titolare Estensione temporale (fine) Estensione temporale (inizio) Tema Partita IVA 